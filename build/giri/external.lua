-- ///////////////////////////////////
-- ///////////////////////////////////
-- ///////////////////////////////////
-- WARNING
-- This file has been auto-generated.
-- Do NOT make modifications directly to it as they will be overwritten!
-- ///////////////////////////////////
-- ///////////////////////////////////
-- ///////////////////////////////////

-- Project names:
local base_name = 'VIOSOWarper'
local component_project_name = base_name
local library_project_name = base_name .. 'Library'
local unittest_project_name = base_name .. 'UnitTest'

-- Projects:
if _ACTION == 'vs2010' then
externalproject( component_project_name )
  location ( project_root .. 'vs2010' )
  uuid ( '8462db4b-f02d-446a-afbd-803f3f784b72' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2010' )
  uuid ( '30cdd53b-185e-45a7-adb8-e47a699c8acd' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2010' )
  uuid ( '3cb101a5-1099-4ecd-8ecf-30b7e0d865d4' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2012' then
externalproject( component_project_name )
  location ( project_root .. 'vs2012' )
  uuid ( '8462db4b-f02d-446a-afbd-803f3f784b72' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2012' )
  uuid ( '30cdd53b-185e-45a7-adb8-e47a699c8acd' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2012' )
  uuid ( '3cb101a5-1099-4ecd-8ecf-30b7e0d865d4' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2013' then
externalproject( component_project_name )
  location ( project_root .. 'vs2013' )
  uuid ( '8462db4b-f02d-446a-afbd-803f3f784b72' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2013' )
  uuid ( '30cdd53b-185e-45a7-adb8-e47a699c8acd' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2013' )
  uuid ( '3cb101a5-1099-4ecd-8ecf-30b7e0d865d4' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2015' then
externalproject( component_project_name )
  location ( project_root .. 'vs2015' )
  uuid ( '8462db4b-f02d-446a-afbd-803f3f784b72' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2015' )
  uuid ( '30cdd53b-185e-45a7-adb8-e47a699c8acd' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2015' )
  uuid ( '3cb101a5-1099-4ecd-8ecf-30b7e0d865d4' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2017' then
externalproject( component_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '8462db4b-f02d-446a-afbd-803f3f784b72' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '30cdd53b-185e-45a7-adb8-e47a699c8acd' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '3cb101a5-1099-4ecd-8ecf-30b7e0d865d4' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2019' then
externalproject( component_project_name )
  location ( project_root .. 'vs2019' )
  uuid ( '8462db4b-f02d-446a-afbd-803f3f784b72' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2019' )
  uuid ( '30cdd53b-185e-45a7-adb8-e47a699c8acd' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2019' )
  uuid ( '3cb101a5-1099-4ecd-8ecf-30b7e0d865d4' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

